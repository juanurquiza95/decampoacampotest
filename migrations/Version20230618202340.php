<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230618202340 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE producto(id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, precio_pesos NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql("INSERT INTO producto(id, nombre, precio_pesos) VALUES(1, 'Monitor', 2000.00);");
        $this->addSql("INSERT INTO producto(id, nombre, precio_pesos) VALUES(2, 'Mouse', 1000.00);");
        $this->addSql("INSERT INTO producto(id, nombre, precio_pesos) VALUES(3, 'Teclado', 400.00);");
        $this->addSql("INSERT INTO producto(id, nombre, precio_pesos) VALUES(4, 'Celular', 1000.00);");
        $this->addSql("INSERT INTO producto(id, nombre, precio_pesos) VALUES(5, 'Auriculares', 2000.00);");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE producto');
    }
}
