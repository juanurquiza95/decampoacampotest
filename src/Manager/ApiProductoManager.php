<?php
namespace App\Manager;

use App\Repository\ProductoRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ApiProductoManager
{
    private $productoRepository;

    public function __construct(ProductoRepository $productoRepository)
    {
        $this->productoRepository = $productoRepository;
    }

    public function getProductosJson(){
        $serializer = $this->getSerializer();

        $productos = $this->productoRepository->getProductos();

        return $serializer->serialize($productos, 'json');
    }

    private function getSerializer(){
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizer = [new ObjectNormalizer()];
        return new Serializer($normalizer, $encoders);
    }
}