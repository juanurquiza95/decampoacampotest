<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Producto
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[Assert\NotNull]
    #[ORM\Column(length: 255)]
    private ?string $nombre = null;

    #[Assert\NotNull]
    #[ORM\Column(name:'precio_pesos',type: 'decimal', precision: 10, scale: 2)]
    private ?float $precioPesos = 0;

    private ?float $precioDolares = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return float
     */
    public function getPrecioPesos(): ?float
    {
        return $this->precioPesos;
    }

    /**
     * @param float $precioPesos
     */
    public function setPrecioPesos(?float $precioPesos): void
    {
        $this->precioPesos = $precioPesos;
    }

    /**
     * @param mixed $precioDolares
     */
    public function setPrecioDolares(?float $precioDolares): void
    {
        $this->precioDolares = $precioDolares;
    }

    /**
     * @return float|null
     */
    public function getPrecioDolares(): ?float
    {
        $valorDolarEnPeso = $_ENV['VALOR_DOLAR_EN_PESOS']; // Assuming the exchange rate is stored in the EXCHANGE_RATE variable in .env

        if ($this->precioPesos !== 0 && $valorDolarEnPeso !== null) {
            return $this->precioPesos * $valorDolarEnPeso;
        }

        return null;
    }
}