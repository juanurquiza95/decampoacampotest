<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Form\ProductoFormType;
use App\Manager\ApiProductoManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiProductoController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/producto/api-get/all', name: 'api_producto_get_listado')]
    public function getProductos(ApiProductoManager $apiProductoManager): JsonResponse
    {
        $productos = $apiProductoManager->getProductosJson();

        return $this->json($productos,headers: ['Content-Type' => 'application/json;charset=UTF-8']);
    }


    #[Route('/producto/post', name: 'api_producto_post')]
    public function create(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $this->json('Invalid JSON', Response::HTTP_BAD_REQUEST);
        }

        $producto = new Producto();

        $form = $this->createForm(ProductoFormType::class, $producto,array('csrf_protection' => false));

        $form->submit($data);

        if (!$form->isValid()) {
            $errors = $this->getErrorsFromForm($form);
            return $this->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        $this->entityManager->persist($producto);
        $this->entityManager->flush();

        return $this->json($producto);
    }

    private function getErrorsFromForm(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors(true, true) as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                $childErrors = $this->getErrorsFromForm($childForm);
                if (!empty($childErrors)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}