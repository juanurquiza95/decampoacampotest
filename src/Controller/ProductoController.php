<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Form\ProductoFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductoController extends AbstractController
{
    #[Route('/producto/listado', name: 'producto_listado')]
    public function listadoAction(): Response
    {
        return $this->render('producto/index.html.twig');
    }

    #[Route('/producto/nuevo', name: 'producto_nuevo')]
    public function nuevoProductoAction(): Response
    {
        return $this->render('producto/new.html.twig',);
    }
}