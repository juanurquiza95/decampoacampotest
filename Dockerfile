FROM composer:2 as composer
FROM php:8.1-apache

RUN apt-get update && \
    docker-php-ext-install mysqli pdo pdo_mysql

RUN apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-install zip

# Set the working directory to /var/www/html
WORKDIR /var/www/html

# Copy the contents of the current directory to /var/www/html in the container
COPY . /var/www/html

COPY --from=composer /usr/bin/composer /usr/local/bin/composer

RUN composer install

RUN a2enmod rewrite && service apache2 restart